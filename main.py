'''
A simple static http server with specified paths
'''

import json
import time
from http.server import SimpleHTTPRequestHandler
import uuid


class JSONHandler(SimpleHTTPRequestHandler):
    """_summary_

    Args:
        SimpleHTTPRequestHandler (_type_): handles the specified paths
    """

    def do_GET(self):
        if self.path == '/json':
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            json_data = {
                "slideshow": {
                    "author": "Yours Truly",
                    "date": "date of publication",
                    "slides": [
                        {
                            "title": "Wake up to WonderWidgets!",
                            "type": "all"
                        },
                        {
                            "items": [
                                "Why <em>WonderWidgets</em> are great",
                                "Who <em>buys</em> WonderWidgets"
                            ],
                            "title": "Overview",
                            "type": "all"
                        }
                    ],
                    "title": "Sample Slide Show"
                }
            }

            self.wfile.write(json.dumps(json_data).encode('utf-8'))

        elif self.path == '/html':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            html_code = """<!DOCTYPE html>
                            <html>
                            <head>
                            </head>
                            <body>
                                <h1>Any fool can write code that a computer can understand. 
                                Good programmers write code that humans can understand.</h1>
                                <p> - Martin Fowler</p>

                            </body>
                            </html>"""
            self.wfile.write(html_code.encode('utf-8'))

        elif self.path == '/uuid':
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            gen_uuid = {
                "uuid": str(uuid.uuid4())
            }
            self.wfile.write(json.dumps(gen_uuid).encode('utf-8'))

        elif self.path.startswith('/delay/'):
            delay_time_str = self.path.split('/')[-1]
            try:
                delay_in_sec = int(delay_time_str)
                time.sleep(delay_in_sec)
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(
                    f"success response in {delay_in_sec}second".encode('utf-8'))
            except ValueError:
                self.send_response(400)
                self.wfile.write("Invalid delay response".encode('utf-8'))

        elif self.path.startswith('/status'):
            try:
                status_code = int(self.path.split('/')[-1])
                if 100 <= status_code <= 599:
                    self.send_response(status_code)
                    self.send_header('Content-type', 'text/plain')
                    self.end_headers()
                    self.wfile.write(
                        f"Response with status code {status_code}".encode('utf-8'))
                else:
                    self.send_response(400)
                    self.wfile.write("Invalid status code".encode('utf-8'))
            except ValueError:
                self.send_response(400)
                self.wfile.write("Invalid status code".encode('utf-8'))


if __name__ == '__main__':
    from http.server import HTTPServer
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, JSONHandler)
    print('Server is running at http://localhost:8000/json')
    httpd.serve_forever()
